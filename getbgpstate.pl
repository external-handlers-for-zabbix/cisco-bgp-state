#getbgpstate.pl
#created by _KUL (Kuleshov)
#version 2016-10-24
#Apache License 2.0


#!/usr/bin/perl
use warnings;
use strict;
use v5.14;
use Data::Dumper;
use Net::SNMP;

if (defined($ARGV[0]) and defined($ARGV[1])) {
my ($session) = Net::SNMP->session(
                            	-timeout => 2,
                            	-retries => 2,
                            	-hostname => "$ARGV[0]",
                            	-community => "$ARGV[1]",
							) or die $!;
		my $mass = $session->get_table(-baseoid => "1.3.6.1.2.1.15.3.1.7");
		my @tmp;
		foreach (keys %{$mass}) {
			push(@tmp,"1.3.6.1.2.1.15.3.1.2.".$mass->{$_});
		}
		my $result = $session->get_request(
									-varbindlist => \@tmp,
								) or die $!;
		my $template;
		my $flag = 0;
		foreach (keys %{$result}) {
			$template = "1.3.6.1.2.1.15.3.1.2.";
			$template .= $ARGV[2] if defined($ARGV[2]);
			if ($_ =~ /$template/) {
				say $result->{$_};
				$flag++;
			}
		}
		say $flag if $flag eq 0;
} else {
	say "Need to run so: getbgpstate.pl <IP host> <COMMUNITY> [(ip BGP neighbor) || (mask ip)]";
}
